function addItem (event) {
    event.preventDefault()
    const name = event.target.name.value
    const message = event.target.post.value
    document.getElementById("name").value = ""
    document.getElementById("post").value = ""

    if (!name || !message) {
        alert('Please enter both author and a message')
    } else {
        // Creating the outer container for a post
        const outerDiv = document.createElement("div")
        outerDiv.classList.add('post-item')

        // Creating the post header
        const header = document.createElement("div")
        header.classList.add('post-header')
        const h2 = document.createElement("h2")
        h2.innerHTML = `${name}`
        header.appendChild(h2)
        outerDiv.appendChild(header)

        // Creating the post message
        const postMessage = document.createElement("div")
        postMessage.innerHTML = `${message}`
        outerDiv.appendChild(postMessage)
    
        // Creating the remove-button
        const button = document.createElement('button')
        button.addEventListener('click', () => {
            postsContainer.removeChild(outerDiv)
        })
        button.textContent = 'Remove'
        outerDiv.appendChild(button)
        
        // Appending the created div to HTML
        const postsContainer = document.getElementById("posts")
        postsContainer.appendChild(outerDiv)
    }
}