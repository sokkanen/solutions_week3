import axios from "axios";

const getData = async () => {
  const response = await axios.get(
      "https://jsonplaceholder.typicode.com/todos/",
  );
  console.log(response.data);
};

getData();
